from tkinter import *
from collections import Counter
from db import DB
from configs import *
import numpy as np
import sys
import time, copy
import sqlite3

class Sudoku:

    def __init__(self, frame, frame2=None):
        self.data = []
        self.data2 = []
        self.buttons = []
        self.buttons2 = []
        self.frame = frame
        self.frame2 = frame2
        self.loopCounter = 0
        self.weirdWay = None
        self.level = 0
        self.d = DB()

        # generate GUI elements
        self.makeBoard(self.frame)
        self.makeBoard(self.frame2)

        self.b1 = Button(master=self.frame, text='solve', command=lambda: self.solve())
        self.b1.pack(side='left', expand=0)

        self.b2 = Button(master=self.frame, text='reset', command=lambda: self.reset())
        self.b2.pack(side='left', expand=0)

        self.b3 = Button(master=self.frame, text='custom Lvl: 304', command=lambda: self.reset(304))
        self.b3.pack(side='left', expand=0)

        self.b4 = Button(master=self.frame, text='custom Lvl: 313', command=lambda: self.reset(313))
        self.b4.pack(side='left', expand=0)

        # this level needs extra work
        self.b5 = Button(master=self.frame, text='custom Lvl: 316', command=lambda: self.reset(316))
        self.b5.pack(side='left', expand=0)


        self.b28 = Button(master=self.frame, text='full test - 500k', command=lambda: self.test(500000, 1))
        self.b28.pack(side='left', expand=0)

        self.b38 = Button(master=self.frame, text='full test - 1M', command=lambda: self.test(1000000, 500000))
        self.b38.pack(side='left', expand=0)


        self.b9 = Button(master=self.frame, text='unsolved', command=lambda: self.testUnsolved())
        self.b9.pack(side='left', expand=0)


        self.one2nine = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9], dtype="int8")

    # make structure
    def generateMatrix(self):
        self.data = np.array(
        [
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8'),
            np.zeros((9,), dtype='int8')
        ],
            np.int8)

    # tk buttons
    def makeBoard(self, parent):
        for i in range(9):
            ff = Frame(master=parent)
            ff.pack(fill=BOTH, expand=0, pady=0)
            if (i+1) % 3 == 0 and i > 0:
                ff2 = Frame(master=parent, bg='black')
                ff2.pack(fill=BOTH, expand=0, pady=2)

            for j in range(9):
                btn = Button(master=ff, text="  ")
                btn.pack(fill='x', side='left', expand=0, ipadx=5, ipady=5)
                if (j+1) % 3 == 0 and j > 0:
                    ff2 = Frame(master=ff, bg='black')
                    ff2.pack(fill=BOTH, side='left', expand=0, pady=2)

                if parent == self.frame:
                    self.buttons.append(btn)
                if parent == self.frame2:
                    self.buttons2.append(btn)

    # show values on buttons extracted from matrix
    def convertMatrixToGUI(self):
        jc = 0
        for i in self.data:
            for j in i:
                if j != 0:
                    self.buttons[jc].config(text=j, bg='darkgrey')
                jc += 1

    # called on button click to start solving
    def solve(self):
        if len(self.data) > 0:
            self.breaks = True
            self.hasBeenInserted = 2
            while self.breaks and self.hasBeenInserted > 0:
                self.breaks = False                                     # indicator if number has been found anywhere to continue looping
                self.hasBeenInserted -= 1
                for i in range(0, 9):                                   # go for 9 rows
                    self.loopCounter += 1
                    # --------------  checking the row -------------------- #
                    row, validRowNumbers = self._getRowColumn(i, 0)         # numbers that can be inserted in that row
                    self.doTheCalc(i, validRowNumbers, row, CALC_ROW, 0)    # check if there's ONLY ONE place in a row available

                    # --------------  checking the column ----------------- #
                    col, validColNumbers = self._getRowColumn(i, 1)         # numbers that can be inserted in that col
                    self.doTheCalc(i, validColNumbers, col, CALC_COL, 1)    # check if there's ONLY ONE place in a col available

                    # --------------  checking the row -------------------- #
                    row, validRowNumbers = self._getRowColumn(i, 0)         # numbers that can be inserted in that row
                    self.doTheCalc2(i, validRowNumbers, row, CALC2_ROW, 0)   # check if all but one number are restricted for specific cell

                    # --------------  checking the column ----------------- #
                    column, validColNumbers = self._getRowColumn(i, 1)      # numbers that can be inserted in that col
                    self.doTheCalc2(i, validColNumbers, column, CALC2_COL, 1)   # check if all but one number are restricted for specific cell

                if self.breaks is False:
                    emptyFieldsLeft = self.checkFinished()                  # checks if there are empty cells
                    if emptyFieldsLeft:
                        self.doTheCalc3()

                        if self.hasBeenInserted == 0:                       # if algorithm can't continue
                            self.validate()
                    else:
                        self.breaks = False
                        self.validate()

        self.loopCounter = 0

    # when cell can be resolved by searching for only place in a row/column where it's allowed, if there are multiple than fails
    def doTheCalc(self, i, validNumbers, row_col, color, switch):

        tmp = []
        for vn in validNumbers:
            if not self.breaks:
                tmp = []                                                            # tmp for valid indexes
                for j in range(0, 9):                                               # checking each cell against number
                    self.loopCounter += 1
                    if row_col[j] == 0:                                             # zero = empty field

                        matrixRestriction = self._getTheMatrix(i, j, switch)        # get the list of 3x3 non zero numbers for rows
                        if len(matrixRestriction) == 8:                             # special case with only one cell left
                            ans = self._completeThe9(i, j, matrixRestriction, switch)
                            if ans != 0:
                                matrixRestriction.append(ans)
                                self.breaks = True

                        RC_Restriction = self._getRCRestriction(j, switch)          # non zero column/row numbers
                        if len(RC_Restriction) == 8:                                # special case with only one cell left
                            ans = self._completeThe9(i, j, RC_Restriction, switch)
                            if ans != 0:
                                RC_Restriction.append(ans)
                                self.breaks = True

                        if not self.breaks and vn not in matrixRestriction and vn not in RC_Restriction:    # check if value can be inserted
                            if self.weirdWay is not None:
                                ansl = self._getWeirdWay(i, j, switch)
                                if vn in ansl or ansl == [0, 0]:
                                    tmp.append((i, j, vn))
                            else:
                                tmp.append((i, j, vn))
            if not self.breaks:
                if len(tmp) == 1:                                                   # if there's only one place in a row where number can be inserted
                    ind1, ind2, val = tmp[0]                                        # extract the values
                    if switch == 1:                                                 # swap values if working with COLUMNS
                        ind1, ind2 = ind2, ind1
                    self.data[ind1][ind2] = val                                     # change value in array
                    if self.level == 0:
                        self.buttons[ind1 * 9 + ind2].config(text=val, bg=color)    # change text on button
                    self.breaks = True
                    self.hasBeenInserted += 1
                    if self.weirdWay is not None:
                        self.weirdWay = None

    # when one cell can be resolved by restricting numbers
    def doTheCalc2(self, i, validNumbers, row_col, color, switch):

        for j in range(0, 9):           # go for 9 cells
            self.loopCounter += 1
            if row_col[j] == 0:         # zero = empty field
                tmp = validNumbers[:]   # copy of valid numbers

                if self.weirdWay is not None:
                    tmp2 = []
                    matrixRestriction = self._getTheMatrix(i, j, switch, self.weirdWay)  # get the list of 3x3 non zero numbers for rows
                    for ih in matrixRestriction:
                        if ih > 9:
                            tmp2.append(ih)
                            matrixRestriction.pop(matrixRestriction.index(ih))

                    c = Counter(tmp2)
                    for o in c:
                        if c[o] == 2:
                            sdat = str(o)
                            matrixRestriction.append(int(sdat[0]))
                            matrixRestriction.append(int(sdat[1]))

                else:
                    matrixRestriction = self._getTheMatrix(i, j, switch)  # get the list of 3x3 non zero numbers for rows

                RC_Restriction = self._getRCRestriction(j, switch)    # non zero column/row numbers

                combined = set(matrixRestriction + RC_Restriction)    # restricted numbers combined

                for ind in combined:
                    try:
                        tmp.pop(tmp.index(ind))
                    except ValueError:
                        pass

                if len(tmp) == 1:
                    num = tmp[0]
                    self.breaks = True
                    self.hasBeenInserted += 1
                    self.weirdWay = None
                    if switch == 0:
                        self.data[i][j] = num
                        if self.level == 0:
                            self.buttons[i * 9 + j].config(text=num, bg=color)  # change text on button
                    else:
                        self.data[j][i] = num
                        if self.level == 0:
                            self.buttons[j * 9 + i].config(text=num, bg=color)  # change text on button

    # weird way - it's wierd bcs thear are lot of conversion from int to string and vice versa, but it works!
    def doTheCalc3(self):
        data = copy.copy(self.data)  # make a copy
        tmp = True
        self.flag = False
        while tmp:
            tmp = False
            for i in range(0, 9):
                self.loopCounter += 1
                # ----------------- row ----------------- #
                row = data[i, :]
                zeroCount = np.count_nonzero(row)
                if 9-zeroCount == 2:
                    nonZeroIndexes = np.nonzero(row)[0]
                    nonZeros = [row[x] for x in nonZeroIndexes]
                    for ijk in nonZeros:
                        if ijk > 9:   # 12 is the lowest, it's combination of available numbers already pushed in previous iteration
                            sijk = str(ijk)
                            nonZeros.append(int(sijk[0]))
                            nonZeros.append(int(sijk[1]))
                    available = [y for y in self.one2nine if y not in nonZeros]
                    if len(available) == 2:
                        a1 = str(available[0])
                        a2 = str(available[1])
                        num = int(a1 + a2)
                        for j in range(0, 9):
                            item = row[j]
                            if item == 0:
                                data[i][j] = num
                                tmp = True
                                self.flag = True  # flag on True if hidden pair was found

                # --------------- column ---------------- #
                column = data[:, i]   # to get the original numbers and avoid those inserted with this code above for rows
                zeroCount2 = np.count_nonzero(column)
                if 9-zeroCount2 == 2:
                    nonZeroIndexes2 = np.nonzero(column)[0]
                    nonZeros2 = [column[x2] for x2 in nonZeroIndexes2]
                    for ijk2 in nonZeros2:
                        if ijk2 > 9:
                            sijk = str(ijk2)
                            nonZeros2.append(int(sijk[0]))
                            nonZeros2.append(int(sijk[1]))
                    available2 = [y2 for y2 in self.one2nine if y2 not in nonZeros2]
                    if len(available2) == 2:
                        a1 = str(available2[0])
                        a2 = str(available2[1])
                        num2 = int(a1 + a2)
                        for j2 in range(0, 9):
                            item2 = column[j2]
                            if item2 == 0:
                                data[j2][i] = num2
                                tmp = True
                                self.flag = True

        # print(data)  to debug it
        self.weirdWay = data[:]
        self.breaks = self.flag

    # last resource
    def _getWeirdWay(self, i, j, switch):
        if switch == 1:
            i, j = j, i
        if self.weirdWay[i][j] > 9:
            first = str(self.weirdWay[i][j])[0]
            second = str(self.weirdWay[i][j])[1]
            return [ int(first), int(second) ]
        else:
            return[0, 0]        # empty column

    # check if puzzle has been solved
    def checkFinished(self):
        flag = False
        for i in range(0, 9):
            self.loopCounter += 1
            row = self.data[i, :]
            try:
                NonZeroCount = np.count_nonzero(row)[0]
            except TypeError:
                pass
            finally:
                NonZeroCount = np.count_nonzero(row)
            if NonZeroCount < 9:
                flag = True
                break
        return flag

    # validate the result
    def validate(self):
        tmp = []
        sumFields = 0
        counter = 0
        suma = 0
        for i in self.data:
            B = np.sum(i)           # 45 is the size of a row/column of correctly inserted elements
            S = np.count_nonzero(i) # number of filled cells

            if B != 45 and S == 9:
                tmp.append(counter)
            else:
                sumFields += B

            counter += 1        # row number, like in 5th row is error, so 4 will be inserted in tmp
            suma += S           # solved fields, of non solved puzzles

        if len(tmp) > 0 and suma == 81:          # if puzzle is solved but with wrong data
            print (self.level, '  \033[91m' + "error in %s row(s)" %(str(tmp)) + '\033[0m')
            self.updateDB(0, 0)

        elif sumFields == 9*45 and len(tmp) == 0:       # if the puzzle is solve
            print(self.level, "  \033[92m validation OK \033[0m")
            self.updateDB(1, 81)

        else:                                           # if the puzzle is not solved, but data are correct (can't continue)
            print(self.level, "  \033[93m not completed \033[0m", suma)
            self.updateDB(0, suma)

    # complete 9th number when 8 are known
    def _completeThe9(self, i, j, matrix, switch, color=THE9_ROW):

        if switch == 1:
            i, j = j, i
            if color == THE9_ROW:
                color = THE9_COL  # special case to distinguish wheter number was filled with row or column search

        if self.data[i][j] == 0:
            num = [x for x in self.one2nine if x not in matrix]
            self.data[i][j] = num[0]
            if self.level == 0:
                self.buttons[i * 9 + j].config(text=num[0], bg=color)  # change text on button
            return num[0]       # this is used to append number in restriction arrays to avoid extra looping
        else:
            return 0            # this will serve as false

    # get the 3x3 submatrix based by position of iterator
    def _getTheMatrix(self, i, j, switch, data=None):
        d = self.data if data is None else data
        if switch == 1:
             i, j = j, i
        tpl = (i // 3, j // 3)      # tuple of submatrix indexes
        if tpl == (0, 0):           # first 3x3 submatrix
            result = np.array([d[0][0], d[0][1], d[0][2], d[1][0], d[1][1], d[1][2], d[2][0], d[2][1], d[2][2]])
        elif tpl == (0, 1):         # second 3x3 submatrix
            result = np.array([d[0][3], d[0][4], d[0][5], d[1][3], d[1][4], d[1][5], d[2][3], d[2][4], d[2][5]])
        elif tpl == (0, 2):         # third 3x3 submatrix
            result = np.array([d[0][6], d[0][7], d[0][8], d[1][6], d[1][7], d[1][8], d[2][6], d[2][7], d[2][8]])
        #-----------------------------------------------------------------------------------------------------#
        elif tpl == (1, 0):         # fourth 3x3 submatrix
            result = np.array([d[3][0], d[3][1], d[3][2], d[4][0], d[4][1], d[4][2], d[5][0], d[5][1], d[5][2]])
        elif tpl == (1, 1):         # fifth 3x3 submatrix
            result = np.array([d[3][3], d[3][4], d[3][5], d[4][3], d[4][4], d[4][5], d[5][3], d[5][4], d[5][5]])
        elif tpl == (1, 2):         # sixth 3x3 submatrix
            result = np.array([d[3][6], d[3][7], d[3][8], d[4][6], d[4][7], d[4][8], d[5][6], d[5][7], d[5][8]])
        #-----------------------------------------------------------------------------------------------------#
        elif tpl == (2, 0):         # seventh 3x3 submatrix
            result = np.array([d[6][0], d[6][1], d[6][2], d[7][0], d[7][1], d[7][2], d[8][0], d[8][1], d[8][2]])
        elif tpl == (2, 1):         # eighth 3x3 submatrix
            result = np.array([d[6][3], d[6][4], d[6][5], d[7][3], d[7][4], d[7][5], d[8][3], d[8][4], d[8][5]])
        elif tpl == (2, 2):         # nineth 3x3 submatrix
            result = np.array([d[6][6], d[6][7], d[6][8], d[7][6], d[7][7], d[7][8], d[8][6], d[8][7], d[8][8]])
        else:
            sys.exit(101)           # invalid submatrix index

        return [v for v in result if v != 0]

    # get row/columns and filter numbers that are not 0
    def _getRCRestriction(self, j, switch):
        RC = self.data[:, j] if switch == 0 else self.data[j, :]
        return [c for c in RC if c != 0]

    # get row or columnd data based on i - loop
    def _getRowColumn(self, i, switch):
        if switch == 0:
            row = self.data[i, :]                               # declare variable for easier read
            valueRowIndexes = np.nonzero(row != 0)[0]           # return index of non-zero number
            valuesRow = [row[ind] for ind in valueRowIndexes]   # list of used numbers
            validRowNumbers = [r for r in self.one2nine if r not in valuesRow]  # numbers that can be inserted in that row
            return row, validRowNumbers
        else:
            column = self.data[:, i]                                # declare variable for easier read
            valueColIndexes = np.nonzero(column != 0)[0]            # return index of non-zero number
            valuesCol = [column[ind2] for ind2 in valueColIndexes]  # list of used numbers (numbers given in start of the game)
            validColNumbers = [c for c in self.one2nine if c not in valuesCol]  # numbers that can be inserted in that col
            return column, validColNumbers

    # resets the layout and data
    def reset(self, level=None):
        for i in self.buttons:
            i.config(text="  ", bg="lightgray")

        for j in self.buttons2:
            j.config(text="  ", bg="lightgray")

        self.generateMatrix()
        if level == 304:
            self.setLevel304()
            self.setResult304()

        elif level == 313:
            self.setLevel313()
            self.setResult313()

        elif level == 316:
            self.setLevel316()
            self.setResult316()

        self.convertMatrixToGUI()


    # -------------------- TEST BLOCK -------------------------#
    # --------------------   for DB --------------------------#
    def test(self, stop, start=1):
        self.getLevelFromDB(stop, start)  # this should preform the test

    """
        - gathering games from DB
        - making empty matrix
        - setting level flag; self.level
        - displaying if neccessary
    """
    def getLevelFromDB(self, stop, start=1):
        for i in range(start, stop+1):
            self.generateMatrix()                   # making empty matrix
            l, p, s, *rest = self.d.getLevel(i)     # gathering data from DB
            self.level = l                          # tracking the level
            self.setPuzzle(p)                       # filling the main matrix
            if stop+1 - start == 1:                 # used to debug only single level
                self.level = 0
                self.convertMatrixToGUI()
                self.setSolution(s)
            self.solve()

    # setting the matrix based on data from DB
    def setPuzzle(self, chars):
        row = 0
        col = 0
        for i in chars:
            self.data[row][col] = int(i)
            col += 1
            if col == 9:
                row += 1
                col = 0

    # filling right sudoku board (results) using data from DB
    def setSolution(self, s):
        for i in range(0, len(s)):
            self.buttons2[i].config(text=s[i])

    # update DB with "flags" of status and found numbers
    # found numbers (fields) is equal to 81 (9x9) when solved
    def updateDB(self, status, fields):
        if self.level > 0:
            self.d.setSolved(self.level, status, fields)

    # testing only unsolved games
    # used when a low amount of 1M games remained unsolved
    def testUnsolved(self):
        unsolved = self.d.getUnsolvedLevels()
        print('total unsolved games: ', len(unsolved))
        for u in unsolved:
            self.generateMatrix()
            l, p, s, *rest = u
            self.level = l
            self.setPuzzle(p)
            self.solve()


    # ----------------------- END BLOCK ------------------------#


    # custom levels to avoid work with DB
    #
    #
    def setLevel304(self):
        self.data[0][1] = 4
        self.data[0][6] = 3
        self.data[1][0] = 3
        self.data[1][2] = 8
        self.data[1][3] = 5
        self.data[1][4] = 7
        self.data[1][6] = 2
        self.data[2][1] = 2
        self.data[2][4] = 4
        self.data[2][5] = 8
        self.data[2][8] = 6
        self.data[3][3] = 4
        self.data[3][7] = 6
        self.data[3][8] = 3
        self.data[4][3] = 1
        self.data[4][5] = 5
        self.data[5][0] = 1
        self.data[5][1] = 3
        self.data[5][5] = 2
        self.data[6][0] = 6
        self.data[6][3] = 7
        self.data[6][4] = 8
        self.data[6][7] = 3
        self.data[7][2] = 7
        self.data[7][4] = 5
        self.data[7][5] = 3
        self.data[7][6] = 6
        self.data[7][8] = 8
        self.data[8][2] = 3
        self.data[8][7] = 2

    def setLevel313(self):
        self.data[0][2] = 9
        self.data[0][5] = 7
        self.data[0][6] = 8
        self.data[1][0] = 6
        self.data[1][4] = 3
        self.data[1][7] = 1
        self.data[1][8] = 2
        self.data[2][1] = 5
        self.data[2][8] = 3
        self.data[3][1] = 7
        self.data[3][2] = 5
        self.data[3][4] = 2
        self.data[4][3] = 1
        self.data[4][5] = 8
        self.data[5][4] = 6
        self.data[5][6] = 9
        self.data[5][7] = 4
        self.data[6][0] = 5
        self.data[6][7] = 6
        self.data[7][0] = 7
        self.data[7][1] = 9
        self.data[7][4] = 8
        self.data[7][5] = 1
        self.data[7][8] = 4
        self.data[8][2] = 8
        self.data[8][3] = 4
        self.data[8][6] = 3

    def setLevel316(self):
        self.data[0][1] = 3
        self.data[0][8] = 8
        self.data[1][0] = 2
        self.data[1][1] = 7
        self.data[1][3] = 3
        self.data[2][1] = 9
        self.data[2][4] = 2
        self.data[2][6] = 4
        self.data[2][7] = 3
        self.data[3][0] = 6
        self.data[3][3] = 1
        self.data[3][4] = 9
        self.data[4][2] = 9
        self.data[4][3] = 2
        self.data[4][5] = 8
        self.data[4][6] = 3
        self.data[5][4] = 5
        self.data[5][5] = 4
        self.data[5][8] = 2
        self.data[6][1] = 2
        self.data[6][2] = 5
        self.data[6][4] = 7
        self.data[6][7] = 6
        self.data[7][5] = 2
        self.data[7][7] = 8
        self.data[7][8] = 4
        self.data[8][0] = 4
        self.data[8][7] = 5
        # self.data[0][6] = 6
        # self.data[0][8] = 5
        # self.data[1][3] = 3
        # self.data[1][7] = 9
        # self.data[2][1] = 8
        # self.data[2][5] = 4
        # self.data[2][8] = 1
        # self.data[3][1] = 4
        # self.data[3][4] = 2
        # self.data[3][6] = 9
        # self.data[4][4] = 5
        # self.data[3][7] = 7
        # self.data[5][1] = 3
        # self.data[5][2] = 1
        # self.data[5][4] = 8
        # self.data[5][7] = 6
        # self.data[6][0] = 9
        # self.data[6][3] = 6
        # self.data[6][7] = 2
        # self.data[7][1] = 1
        # self.data[7][5] = 7
        # self.data[8][0] = 5
        # self.data[8][2] = 4


    # result of the matrix, to be shown in right sudoku board
    def setResult304(self):
        self.data2 = np.array([
            7,4,9,6,2,1,3,8,5,3,6,8,5,7,9,2,1,4,5,2,1,3,4,8,9,7,6,2,8,5,4,9,7,1,6,3,9,7,6,1,3,5,8,4,2,
            1,3,4,8,6,2,7,5,9,6,9,2,7,8,4,5,3,1,4,1,7,2,5,3,6,9,8,8,5,3,9,1,6,4,2,7
        ], dtype="int8")

        for i in range(0, len(self.data2)):
            self.buttons2[i].config(text=self.data2[i])

    def setResult313(self):
        self.data2 = np.array([
            4,3,9,2,1,7,8,5,6,6,8,7,5,3,9,4,1,2,2,5,1,8,4,6,7,9,3,3,7,5,9,2,4,6,8,1,9,4,6,1,7,8,2,3,5,
            8,1,2,3,6,5,9,4,7,5,2,4,7,9,3,1,6,8,7,9,3,6,8,1,5,2,4,1,6,8,4,5,2,3,7,9
        ], dtype="int8")

        for i in range(0, len(self.data2)):
            self.buttons2[i].config(text=self.data2[i])

    def setResult316(self):
        pass


def main():
    root = Tk()
    root.geometry("1000x600")
    f = Frame(master=root, width=600, height=600)
    f2 = Frame(master=root, width=600, height=600)

    s = Sudoku(f, f2)
    s.generateMatrix()

    f.pack(expand=0, fill=BOTH, side='left')
    f2.pack(expand=0, fill=BOTH, side='left',  padx=10)

    root.mainloop()

# stupid call,  i know, but i don't care atm
main()
