import sqlite3
import sys


class DB:

    def getLevel(self, lvl):
        try:
            self.conn = sqlite3.connect('liteDB.db')
            self.cursor = self.conn.cursor()
            query = 'select * from sudoku where id=?'
            self.cursor.execute(query, (lvl, ))
            result = self.cursor.fetchone()
            # print(result)
            return result
            self.conn.close()
        except:
            sys.exit(200)

    def getUnsolvedLevels(self):
        try:
            self.conn = sqlite3.connect('liteDB.db')
            self.cursor = self.conn.cursor()
            query = 'select * from sudoku where solved=0'
            self.cursor.execute(query, )
            result = self.cursor.fetchall()
            # print(result)
            return result
            self.conn.close()
        except:
            sys.exit(204)

    def getTotalSolved(self):
        try:
            self.conn = sqlite3.connect('liteDB.db')
            self.cursor = self.conn.cursor()
            query = 'select count(id) from sudoku where solved=1'
            self.cursor.execute(query, )
            result = self.cursor.fetchone()
            print(result[0])
            return result[0]
            self.conn.close()
        except:
            sys.exit(205)

    def getTotalFields(self):
        try:
            self.conn = sqlite3.connect('liteDB.db')
            self.cursor = self.conn.cursor()
            query = 'select sum(fields) from sudoku'
            self.cursor.execute(query, )
            result = self.cursor.fetchone()
            print(result[0])
            return result[0]
            self.conn.close()
        except:
            sys.exit(202)

    def setSolved(self, level, solved, fields):
        try:
            self.conn = sqlite3.connect('liteDB.db')
            self.cursor = self.conn.cursor()
            query = 'update sudoku set solved=?, fields=? where id=?'
            self.cursor.execute(query, (solved, fields, level))
            self.conn.commit()
            self.conn.close()
        except:
            sys.exit(203)


if __name__ == '__main__':
    d = DB()
    # l, p, s, *rest = d.getLevel(1)

    # print("leveL %s, puzzle : %s, solution : %s" %(l, p ,s))
    d.getTotalSolved()
    # d.setSolved(1, 1, 30)
    # d.getTotalSolved()
    d.getTotalFields()
    d.getUnsolvedLevels()