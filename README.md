# sudoku

### Description

This is my implementation of sudoku solving algorithms written in Python 3.
There are 1 milion games that can be used for testing algorithms, which you can found in this link
https://www.kaggle.com/bryanpark/sudoku from where i had downloaded it or in the resources folder as well.

### HOW TO
There is a <b>liteDB.db</b> , database file, with those 1M games imported. Code in db.py script is used for managing the
database.
<br><b>Game.py</b> is a main file where all of the magic happens.
Besides those 1M games, there are, currently, 3 more games that are gathered from different source. They are called level 304, 313 and 316. 
Why is this important, it's because level 316 still can't be solve, the algorithm needs improvement.
<br>
<b>configs.py</b> is a file that contains colors that are used while filling the GUI. Different colors are used so that
you can always tell which part of the code has inserted the number.
<br>
If you want to go step-by-step then remove <b>while</b> loop from game.py -> solve () and click on solve button as many times as you want.

<br>
<br>
Note: while testing those 1M, variable <b> self.level</b> is set to database field  'id' which is never 0 (1-1M) which will prevent script 
to display updates on GUI e.g. speed up the process. If you want to test only one level, and see the GUI filling with numbers
then set level to 0 (it's 0 - by default).


### Requirements


<ul>
    <li>numpy</li>
    <li>tkinter</li>
    <li>sqlite 3</li>
</ul>